<!DOCTYPE html>
<html lang="zxx">

   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/flaticon.css">
      <link rel="stylesheet" href="assets/css/remixicon.css">
      <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="assets/css/swiper-min.css">
      <link rel="stylesheet" href="assets/css/magnific-popup.css">
      <link rel="stylesheet" href="assets/css/fancybox.css">
      <link rel="stylesheet" href="assets/css/odometer.css">
      <link rel="stylesheet" href="assets/css/aos.css">
      <link rel="stylesheet" href="assets/css/style.css">
      <link rel="stylesheet" href="assets/css/responsive.css">
      <title>Ipanoji Garden Services | Home</title>
      <link rel="icon" type="image/png" href="assets/img/favicon.png">
   </head>
   <body>
      <div class="preloader js-preloader">
         <img src="assets/img/preloader.gif" alt="Image">
      </div>
      <div class="page-wrapper">
         <header class="header-wrap style1">
            <div class="container">
               <div class="header-top">
                  <div class="close-header-top xl-none">
                     <button type="button">
                     <i class="ri-close-line"></i>
                     </button>
                  </div>
                  <div class="row align-items-center">
                     <div class="col-xl-8 col-lg-12">
                        <div class="header-top-left">
                           <div class="contact-item">
                              <span> <i class="flaticon-call-1"></i></span>
                              <a href="tel:8882222288">0673454567</a>
                           </div>
                           <div class="contact-item">
                              <span><i class="flaticon-envelope"></i></span>
                              <a href="#">enquiry@ipanoji.co.za</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-4 col-lg-12">
                        <div class="header-top-right">
                           <div class="select-lang">
                              <i class="ri-earth-fill"></i>
                              <div class="navbar-option-item navbar-language dropdown language-option">
                                 <button class="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <span class="lang-name"></span>
                                 </button>
                                 <div class="dropdown-menu language-dropdown-menu">
                                    <a class="dropdown-item" href="#">
                                    <img src="assets/img/uk.png" alt="flag">
                                    English
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <ul class="social-profile style1 list-style">
                              <li>
                                 <a target="_blank" href="https://facebook.com/">
                                 <i class="ri-facebook-fill"></i>
                                 </a>
                              </li>

                           </ul>
                        </div>
                        <a href="https://wa.me/+27670595276" target="_blank" class="btn style1 xl-none">Request A Quote <i class="flaticon-right-arrow"></i></a>
                     </div>
                  </div>
               </div>
               <div class="header-bottom">
                  <nav class="navbar navbar-expand-md navbar-light">
                     <a class="navbar-brand" href="{{url('/')}}">
                     <img src="/assets/img/logo.png" width="143" height="51" alt="logo">
                     </a>
                     <div class="collapse navbar-collapse main-menu-wrap" id="navbarSupportedContent">
                        <div class="menu-close xl-none">
                           <a href="javascript:void(0)"> <i class="ri-close-line"></i></a>
                        </div>
                        <ul class="navbar-nav ms-auto">
                           <li class="nav-item">
                              <a href="{{url('/')}}" class="nav-link active">
                              Home
                              </a>
                           </li>


                           <li class="nav-item">
                              <a href="https://wa.me/+27670595276" target="_blank" class="nav-link">Contact Us</a>
                           </li>
                        </ul>
                        <div class="others-options  lg-none">
                           <div class="option-item">
                              <button class="searchbtn" type="button">
                              <i class="flaticon-search-1"></i>
                              </button>
                           </div>
                           <div class="option-item">
                              <a href="https://wa.me/+27670595276" target="_blank" class="btn style1">Request A Quote <i class="flaticon-right-arrow"></i></a>
                           </div>
                        </div>
                     </div>
                  </nav>
                  <div class="mobile-bar-wrap">
                     <div class="mobile-top-bar xl-none">
                        <span></span>
                        <span></span>
                        <span></span>
                     </div>
                     <button class="searchbtn xl-none" type="button">
                     <i class="flaticon-search-1"></i>
                     </button>
                     <div class="mobile-menu xl-none">
                        <a href="javascript:void(0)"><i class="ri-menu-line"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="search-area">
               <div class="container">
                  <button type="button" class="close-searchbox">
                  <i class="ri-close-line"></i>
                  </button>
                  <form action="#">
                     <div class="form-group">
                        <input type="search" placeholder="Search Here" autofocus>
                     </div>
                  </form>
               </div>
            </div>
         </header>
         <section class="hero-wrap style1 hero-bg-1 bg-f">
            <div class="container">
               <div class="row gx-5 align-items-center">
                  <div class="col-lg-6">
                     <div class="hero-content">
                        <span data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">DEDICATED GARDEN SERVICES</span>
                        <h1 data-aos="fade-right" data-aos-duration="1000" data-aos-delay="600">We are committed to making beautiful homes</h1>
                        <p data-aos="fade-right" data-aos-duration="1000" data-aos-delay="900">Ipanoji offers very affordable and quality garden services in the Pietermaritzburg area.We have a team of experienced gardeners who will ensure that your home or place of work is maintained properly in time.</p>
                        <div class="hero-btn">
                           <a href="https://wa.me/+27670595276" target="_blank" class="btn style1">Whatsapp Us <i class="flaticon-right-arrow-1"></i></a>

                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="hero-img-wrap">
                        <div class="hero-promo-box one">
                           <img src="/assets/img/hero/pot-1.png" alt="Image">
                           <div class="promo-view">
                              <i class="flaticon-add"></i>
                              <span class="ripple"></span>
                              <div class="hero-promo-info">
                                 <span><i class="flaticon-gardening-2"></i></span>
                                 <h6>Gardening</h6>
                              </div>
                           </div>
                        </div>
                        <div class="hero-promo-box two">
                           <img src="assets/img/hero/pot-2.png" alt="Image">
                           <div class="promo-view">
                              <i class="flaticon-add"></i>
                              <span class="ripple"></span>
                              <div class="hero-promo-info">
                                 <span><i class="flaticon-sapling"></i></span>
                                 <h6>Plants Planted</h6>
                              </div>
                           </div>
                        </div>
                        <div class="hero-promo-box three">
                           <img src="assets/img/hero/pot-3.png" alt="Image">
                           <div class="promo-view">
                              <i class="flaticon-add"></i>
                              <span class="ripple"></span>
                              <div class="hero-promo-info">
                                 <span><i class="flaticon-farming"></i></span>
                                 <h6>Garden Setup</h6>
                              </div>
                           </div>
                        </div>
                        <img src="/assets/img/home.jpg" alt="Image" class="hero-img">
                     </div>
                  </div>
               </div>
            </div>
         </section>

         <section class="service-wrap ptb-100 bg-spring-wood">
            <img src="assets/img/tub-2.png" alt="Image" class="section-img style2">
            <div class="container">
               <div class="row">
                  <div class="col-xl-6 offset-xl-3 col-lg-10 offset-lg-1">
                     <div class="section-title style1 text-center mb-50">
                        <span>OUR SERVICES</span>
                        <h2>What we do</h2>
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center">
                  <div class="col-xl-4 col-lg-6 col-md-6">
                     <div class="service-card style1">
                        <div class="service-img">
                           <img src="assets/img/service/service-1.jpg" alt="Image">
                        </div>
                        <div class="service-info">
                           <h3 class="service-title">
                              <a href="service-details.html">
                              <span><i class="flaticon-landscape"></i></span>
                              Garden Maintenance
                              </a>
                           </h3>
                           <p>We are ready to maintain your garden and keep your grass short at all times.</p>
                           <a href="https://wa.me/+27670595276" target="_blank" class="btn style1">Whatsapp for Quote<i class="flaticon-right-arrow-1"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6">
                     <div class="service-card style1">
                        <div class="service-img">
                           <img src="/assets/img/car.jpg" alt="Image">
                        </div>
                        <div class="service-info">
                           <h3 class="service-title">
                              <a href="service-details.html">
                              <span><i class="flaticon-plant-1"></i></span>
                              Rubish Removals
                              </a>
                           </h3>
                           <p>We remove and despose all rubish of all sorts</p>
                            <a href="https://wa.me/+27670595276" target="_blank" class="btn style1">Whatsapp for Quote<i class="flaticon-right-arrow-1"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-md-6">
                     <div class="service-card style1">
                        <div class="service-img">
                           <img src="/assets/img/tree.jpg" alt="Image">
                        </div>
                        <div class="service-info">
                           <h3 class="service-title">
                              <a href="service-details.html">
                              <span><i class="flaticon-park"></i></span>
                             Tree Felling
                              </a>
                           </h3>
                           <p>We take care of all sorts of trees and despose afterwards.</p>
                          <a href="https://wa.me/+27670595276" target="_blank" class="btn style1">Whatsapp for Quote<i class="flaticon-right-arrow-1"></i></a>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </section>


         <footer class="footer-wrap bg-spring-wood">
            <div class="footer-shape-1"></div>
            <img src="/assets/img/logo.png"  width="143" height="51" alt="Image" class="footer-shape-2 lg-none">
            <div class="footer-top">
               <div class="container">
                  <div class="row pt-100 pb-75">
                     <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                        <div class="footer-widget">
                           <a href="{{url('/')}}" class="footer-logo">
                           <img src="assets/img/logo.png" width="143" height="51" alt="Image">
                           </a>
                           <p class="comp-desc">
                              Ipanoji offers very affordable and quality garden services in the Pietermaritzburg area.We have a team of experienced gardeners who will ensure that your home or place of work is maintained properly in time.
                           </p>
                           <div class="social-link">
                              <h6>Follow Us:</h6>
                              <ul class="social-profile list-style style1">
                                 <li>
                                    <a target="_blank" href="https://facebook.com/">
                                    <i class="ri-facebook-fill"></i>
                                    </a>
                                 </li>

                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6">
                        <div class="footer-widget">
                           <h3 class="footer-widget-title">Company</h3>
                           <ul class="footer-menu list-style">
                              <li>
                                 <a href="https://wa.me/+27670595276" target="_blank">
                                 <i class="ri-arrow-right-s-fill"></i>
                                 Contact Us
                                 </a>
                              </li>

                           </ul>
                        </div>
                     </div>

                     <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
                        <div class="footer-widget">
                           <h3 class="footer-widget-title">Get In Touch</h3>
                           <ul class="contact-info list-style">
                              <li>
                                 <i class="flaticon-maps-and-flags"></i>
                                 <h6>Location</h6>
                                 <p>47 Milliken Road Presbury, Pietermaritzburg, KZN, South Africa</p>
                              </li>
                              <li>
                                 <i class="flaticon-email-2"></i>
                                 <h6>Email</h6>
                                 <a href="#">enquiry@ipanoji.co.za</a>
                              </li>
                              <li>
                                 <i class="flaticon-call"></i>
                                 <h6>Phone</h6>
                                 <a href="tel:13454567877">0670595276 | 0624119203</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-bottom">
               <div class="container">
                  <div class="row align-items-center">
                     <div class="col-lg-6 col-md-7">
                        <p class="copyright-text">
                           <i class="ri-copyright-line"></i>2022 <a href="{{url('/')}}">Ipanoji</a>. Designed by <a href="https://droitix.co.za" target="_blank">Droitix Web & Systems</a>
                        </p>
                     </div>
                     <div class="col-lg-6 col-md-5">
                        <ul class="footer-bottom-menu list-style">
                           <li><a target="_blank" href="{{url('/')}}">Terms &amp; Conditions</a></li>
                           <li><a target="_blank" href="{{url('/')}}">Privacy Policy</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <a href="javascript:void(0)" class="back-to-top bounce"><i class="ri-arrow-up-s-line"></i></a>
      <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/bootstrap.bundle.min.js"></script>
      <script src="assets/js/form-validator.min.js"></script>
      <script src="assets/js/contact-form-script.js"></script>
      <script src="assets/js/aos.js"></script>
      <script src="assets/js/owl.carousel.min.js"></script>
      <script src="assets/js/swiper-min.js"></script>
      <script src="assets/js/jquery-magnific-popup.js"></script>
      <script src="assets/js/fancybox.js"></script>
      <script src="assets/js/odometer.min.js"></script>
      <script src="assets/js/progressbar.min.js"></script>
      <script src="assets/js/jquery.appear.js"></script>
      <script src="assets/js/tweenmax.min.js"></script>
      <script src="assets/js/main.js"></script>
   </body>
   <!-- Mirrored from templates.hibootstrap.com/guze/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 21 Feb 2022 07:38:48 GMT -->
</html>
